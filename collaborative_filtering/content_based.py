import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.preprocessing import FunctionTransformer

# Example DataFrame (replace with your actual data)
data = pd.read_csv('pathplan/database/data_cleaning/filtered_reviews.csv')

df = pd.DataFrame(data)

# Initialize TfidfVectorizer with your preprocessing function
def preprocess_review(review):
    # Example: lowercase and remove punctuation
    return review.lower().replace('.', '').replace('!', '').replace(',', '')

vectorizer = TfidfVectorizer(preprocessor=preprocess_review)

# Fit and transform the reviews
review_vectors = vectorizer.fit_transform(df['Review'])

def recommend_based_on_review(new_review, city, n_places=5):
    new_review_cleaned = preprocess_review(new_review)
    new_review_vector = vectorizer.transform([new_review_cleaned])
    
    # Filter reviews based on the specified city
    city_reviews = df[df['City'] == city]['Review']
    city_review_vectors = review_vectors[df['City'] == city]
    
    # Calculate cosine similarity between the new review vector and filtered review vectors
    similarities = cosine_similarity(new_review_vector, city_review_vectors)
    
    # Get top n similar reviews
    top_indices = similarities.argsort()[0][-n_places:]
    recommended_places = df.iloc[top_indices]['Place'].unique()
    
    return recommended_places

# Example usage
new_user_review = "I love historical places with beautiful architecture."
input_city = 'Mumbai'
content_based_recommendations = recommend_based_on_review(new_user_review, input_city)
print("Recommended places in", input_city, "based on review:", content_based_recommendations)