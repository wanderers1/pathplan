from sklearn.neighbors import NearestNeighbors
import pandas as pd
import re
import nltk
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from sklearn.feature_extraction.text import TfidfVectorizer

# Ensure NLTK resources are downloaded
nltk.download('stopwords')
nltk.download('wordnet')

# Load your CSV file into a DataFrame
file_path = 'pathplan/database/data_cleaning/filtered_reviews.csv'
df = pd.read_csv(file_path)

# Initialize stopwords and lemmatizer
stop_words = set(stopwords.words('english'))
lemmatizer = WordNetLemmatizer()

# Preprocess review text
def preprocess_review(review):
    review = re.sub(r'\W', ' ', str(review))
    review = review.lower()
    review = ' '.join([lemmatizer.lemmatize(word) for word in review.split() if word not in stop_words])
    return review

# Apply preprocessing to reviews
df['Cleaned_Review'] = df['Review'].apply(preprocess_review)

# Use Email as a user identifier
df['User'] = df['Email']

# Handle duplicates by averaging the ratings for the same user, place, and city
df = df.groupby(['User', 'Place', 'City'], as_index=False).agg({'Rating': 'mean', 'Cleaned_Review': ' '.join})

# Filter places based on the city_input
def filter_places_by_city(df, city_input):
    return df[df['City'] == city_input]

# Function to recommend places for a given user in a specific city
def recommend_places_for_user(user_email, city_input, n_neighbors=10):
    # Filter the DataFrame based on the city_input
    df_city = filter_places_by_city(df, city_input)
    
    # If city_input is not found in the dataset, raise an error
    if city_input not in df['City'].unique():
        raise ValueError(f"City '{city_input}' not found in the dataset.")
    
    # If user_email is not found in the filtered city data, continue without raising an error
    if user_email not in df_city['User'].values:
        print(f"Warning: User email '{user_email}' not found in the data for city '{city_input}'. Continuing with recommendations based on city data.")
    
    # Vectorize the cleaned reviews for the filtered city data
    vectorizer = TfidfVectorizer()
    review_vectors_city = vectorizer.fit_transform(df_city['Cleaned_Review'])
    
    # Fit the NearestNeighbors model on the filtered city data
    model_knn = NearestNeighbors(metric='cosine', algorithm='brute')
    model_knn.fit(review_vectors_city)
    
    # Get the index of the user in the filtered city DataFrame
    if user_email in df_city['User'].values:
        user_index = df_city[df_city['User'] == user_email].index[0]
    else:
        user_index = None
    
    # Get recommendations based on the user_index or randomly if user_index is None
    if user_index is not None:
        distances, indices = model_knn.kneighbors(review_vectors_city[user_index], n_neighbors=n_neighbors + 1)
    else:
        # If user_email not found in df_city, recommend based on random data
        random_user_index = 0  # Replace with logic to choose a random user or fallback strategy
        distances, indices = model_knn.kneighbors(review_vectors_city[random_user_index], n_neighbors=n_neighbors + 1)
    
    similar_users_indices = indices.flatten()[1:]  # Exclude the user itself
    
    visited_places_target_user = set(df_city[df_city['User'] == user_email]['Place'].values) if user_index is not None else set()
    visited_places_by_similar_users = set()
    
    # Collect visited places by similar users in the same city
    for idx in similar_users_indices:
        similar_user_email = df_city.iloc[idx]['User']
        places_visited_by_similar_user = set(df_city[df_city['User'] == similar_user_email]['Place'].values)
        visited_places_by_similar_users.update(places_visited_by_similar_user)
    
    # Filter out similar users who have visited the same places as the target user in the same city
    filtered_similar_users = [idx for idx in similar_users_indices if not visited_places_target_user.intersection(set(df_city.iloc[idx]['Place'] for idx in similar_users_indices))]
    
    recommended_places = set()
    
    # Aggregate places from filtered similar users in the same city
    for idx in filtered_similar_users:
        similar_user_email = df_city.iloc[idx]['User']
        places_visited_by_similar_user = set(df_city[df_city['User'] == similar_user_email]['Place'].values)
        
        # Update recommended places if the user hasn't visited those places in the same city
        recommended_places.update(places_visited_by_similar_user - visited_places_target_user)
    
    return list(recommended_places)

# Example usage
user_email = 'sureshdarshit@example.com'
city_input = 'Mumbai'  # Replace with the city you want to recommend places for
recommended_places = recommend_places_for_user(user_email, city_input)
print(f"Recommended places in {city_input} for {user_email}: {recommended_places}")
