questions = [
    "What type of place are you interested in?",
    "Do you enjoy learning about history?",
    "Are you interested in religious sites?",
    "Do you prefer outdoor or indoor activities?",
    "Are you looking for a place with natural beauty?",
    "Are you looking for budget-friendly places?",
    "Are you looking for family-friendly places?",
    "Do you prefer places with guided tours?",
    "Are you interested in adventure activities?",
    "Do you prefer places with cultural significance?"
]

options = [
    ["Historical_Place", "Museum", "Monument", "Cultural_Center"],
    ["Yes", "No"],
    ["Temple", "Church", "Mosque", "None"],
    ["Outdoor_Activity", "Indoor_Activity"],
    ["National_Park", "Botanical_Garden", "Beach", "Lake"],
    ["Yes", "No"],
    ["Yes", "No"],
    ["Yes", "No"],
    ["Yes", "No"],
    ["Yes", "No"]
]
