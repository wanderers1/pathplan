from sklearn.neighbors import NearestNeighbors
import pandas as pd
import re
import nltk
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from sklearn.feature_extraction.text import TfidfVectorizer

# Ensure NLTK resources are downloaded
nltk.download('stopwords')
nltk.download('wordnet')

# Load your CSV file into a DataFrame
file_path = 'pathplan/database/data_cleaning/filtered_reviews.csv'
df = pd.read_csv(file_path)

# Print column names to verify 'City' is present
print("Column Names:", df.columns)

# Initialize stopwords and lemmatizer
stop_words = set(stopwords.words('english'))
lemmatizer = WordNetLemmatizer()

# Preprocess review text
def preprocess_review(review):
    review = re.sub(r'\W', ' ', str(review))
    review = review.lower()
    review = ' '.join([lemmatizer.lemmatize(word) for word in review.split() if word not in stop_words])
    return review

# Apply preprocessing to reviews
df['Cleaned_Review'] = df['Review'].apply(preprocess_review)

# Use Email as a user identifier
df['User'] = df['Email']

# Handle duplicates by averaging the ratings for the same user and place
df = df.groupby(['User', 'Place', 'City'], as_index=False).agg({'Rating': 'mean', 'Cleaned_Review': ' '.join})

# Print column names after grouping
print("Column Names after grouping:", df.columns)

# Create the user-item matrix
user_item_matrix = df.pivot(index='User', columns='Place', values='Rating').fillna(0)

# Vectorize the cleaned reviews
vectorizer = TfidfVectorizer()
review_vectors = vectorizer.fit_transform(df['Cleaned_Review'])

# Fit the model for items
item_knn = NearestNeighbors(metric='cosine', algorithm='brute')
item_knn.fit(review_vectors)

# Function to recommend places similar to a given place in a specific city
def recommend_similar_places(place_name, city_name, n_neighbors=5):
    # Debug: Check if 'City' is in DataFrame columns
    print("Column Names:", df.columns)
    
    # Find similar places based on the item KNN model for the specified place name
    filtered_df = df[df['Place'] == place_name]
    
    if filtered_df.empty:
        return f"No data found for {place_name} in the dataset."
    
    place_index = filtered_df.index[0]
    distances, indices = item_knn.kneighbors(review_vectors[place_index], n_neighbors=n_neighbors+1)
    
    similar_places_indices = indices.flatten()
    
    # Exclude the input place itself from recommendations
    similar_places = [df.iloc[idx]['Place'] for idx in similar_places_indices if df.iloc[idx]['Place'] != place_name]
    
    # Return top n_neighbors similar places
    return similar_places[:n_neighbors]

# Example usage
city_name = 'Mumbai'  # Specify the city name from your dataset
place_name = 'Agra Fort'  # Specify the place name
similar_places = recommend_similar_places(place_name, city_name)
print(f"Places similar to {place_name} in {city_name}: {similar_places}")


# Example usage
city_name = 'Mumbai'  # Specify the city name from your dataset
place_name = 'Agra Fort'  # Specify the place name
similar_places = recommend_similar_places(place_name, city_name)
print(f"Places similar to {place_name} in {city_name}: {similar_places}")
