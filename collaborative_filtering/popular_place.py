def recommend_popular_places(n_places=5):
    popular_places = df.groupby('Place').mean()['Rating'].sort_values(ascending=False).head(n_places).index
    return popular_places

# Example usage
popular_places = recommend_popular_places()
print("Recommended popular places:", popular_places)
