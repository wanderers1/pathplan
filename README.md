<img src='app/static/images/logo-without-bg.png'>

## Travel Planner Project Description

An interactive travel planner website where users can explore local attractions for different cities and create personalized itineraries. The website incorporates a collaborative filtering algorithm to recommend tourist attractions based on user ratings.

### Potential Learning

- API Integration
- Web Development
- Database Management
- Collaborative Filtering

### Workflow

1. **User Input:**
   - City and Date: User inputs the destination city and travel dates.

2. **Data Retrieval:**
   - **Attractions and Activities:** Use datasets to fetch popular attractions and activities in the city.
   - **User Reviews and Ratings:** Collect user review data from travel review platforms.

3. **Collaborative Filtering for Recommendations:**
   - **User Review Dataset:** Create a dataset with user reviews, ratings, and visited destinations.
   - **Algorithm:** Implement collaborative filtering to recommend attractions and activities based on similar user preferences and historical data.

4. **User Selection and Route Generation:**
   - **Recommendation Display:** Show recommended attractions and activities to the user.
   - **User Selection:** Allow the user to select attractions and activities for each day of their trip.
   - **Route Optimization:** Use the Google Maps API to generate optimized routes for the selected places.

### Tech Stack

- **Front-End:** HTML, CSS, JavaScript
- **Back-End:** Python, Flask
- **Database:** MySQL
- **APIs:**
  - Google Maps API
  - OpenWeatherMap API
- **AI:** Scikit-Learn

### Timeline

**Week 1:**
- Backend Development
  - Integrate Google Maps and OpenWeatherMap APIs
  - Design database schema (SQLite for simplicity)
- Data Collection and Preparation
  - Dataset: Indian Places to Visit Reviews Data
  - Implement basic data processing and storage

- Frontend Development
  - Create basic web pages (HTML, CSS)
  - Implement interactive elements (JavaScript)

**Week 2:**
- Machine Learning Basics
  - Implement basic collaborative filtering using scikit-learn
  - Test with sample data
- Advanced Backend Features
  - Integrate machine learning models with Flask
  - Finalize Frontend
  - Integrate Google Maps for route visualization
  - Connect frontend to backend via API calls
- Testing
  - Thoroughly test all features
  - Debug and fix issues

### Future Scope

- Optimize travel plans considering distance, cost, and user preferences using simulated annealing.
- Integrate flight booking APIs.
- Suggest accommodations.

### References

- Collaborative Filtering Recommender Systems: [ResearchGate Link](https://www.researchgate.net/publication/200121027_Collaborative_Filtering_Recommender_Systems)
