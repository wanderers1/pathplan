document.getElementById('itineraryForm').addEventListener('submit', function(event) {
    event.preventDefault();
    
    const destination = document.getElementById('destination').value;
    const startDate = document.getElementById('startDate').value;
    const endDate = document.getElementById('endDate').value;
    const people = document.getElementById('people').value;
    
    if (destination && startDate && endDate && people) {
        alert(`Itinerary Planned:\nDestination: ${destination}\nStart Date: ${startDate}\nEnd Date: ${endDate}\nNumber of People: ${people}`);
    } else {
        alert('Please fill in all fields.');
    }
  });