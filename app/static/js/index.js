document.addEventListener('DOMContentLoaded', function () {
    // Initialize the map
    function initMap() {
        const map = new google.maps.Map(document.getElementById('map'), {
            zoom: 7,
            center: { lat: -34.397, lng: 150.644 }
        });
        const directionsService = new google.maps.DirectionsService();
        const directionsRenderer = new google.maps.DirectionsRenderer();
        directionsRenderer.setMap(map);

        // Update route when places are reordered
        const onChangeHandler = function () {
            calculateAndDisplayRoute(directionsService, directionsRenderer);
        };

        // Event listener for sorting changes
        document.querySelector('.planner-list').addEventListener('sortupdate', onChangeHandler);

        calculateAndDisplayRoute(directionsService, directionsRenderer);
    }

    function calculateAndDisplayRoute(directionsService, directionsRenderer) {
        const waypts = [];
        const items = document.querySelectorAll('.planner-list .place');
        items.forEach((item) => {
            waypts.push({
                location: item.dataset.place,
                stopover: true
            });
        });

        directionsService.route(
            {
                origin: "Start Location",
                destination: "End Location",
                waypoints: waypts,
                optimizeWaypoints: true,
                travelMode: google.maps.TravelMode.DRIVING
            },
            (response, status) => {
                if (status === "OK") {
                    directionsRenderer.setDirections(response);
                } else {
                    window.alert("Directions request failed due to " + status);
                }
            }
        );
    }

    // Initialize map
    initMap();
});
