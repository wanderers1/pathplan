from flask import Blueprint, render_template
from flask_login import login_required, current_user

views = Blueprint('views', __name__)

@views.route('/')
# @login_required
def home():
    return render_template("index.html")

@views.route('/itinerary_page')
@login_required
def itinerary_page():
    return render_template('itinerary_page.html')
