from flask import Blueprint, render_template, request, flash, redirect, url_for
from .models import User
from . import db 
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import logout_user, login_required, login_user, current_user


auth = Blueprint('auth', __name__)

@auth.route('/login', methods = ['GET', 'POST'])
def login():
    if request.method == 'POST':
        email = request.form.get('email')
        password = request.form.get('password')

    # check if the user with the email exists
        user = User.query.filter_by(email = email).first()
        if user:
            if check_password_hash(user.password, password):
                # flash('logged in successfully', category='success')
                login_user(user, remember = True)
                return redirect(url_for('views.home'))
            else:
                flash('Incorrect password, try again', category= 'error')
        else:
            flash('email does not exist', category = 'error')
    return render_template("login.html")

@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('views.home'))

@auth.route('/signup',  methods = ['GET', 'POST'])
def signup():
    if request.method == 'POST':
        email = request.form.get('email')
        firstname = request.form.get('firstname')
        password1 = request.form.get('password1')
        password2 = request.form.get('password2')
        # define user
        new_user = User(email = email, firstname = firstname, password = generate_password_hash(password1, method = 'pbkdf2:sha256'))
        # add user to database
        db.session.add(new_user)
        db.session.commit()
        login_user(new_user, remember = True)
        # redirect to home page once user sign ups
        return redirect(url_for('views.home'))
    

# TO FLASH MESSAGE, THERE NEEDS TO BE A FUNC ADDED IN template.html
        # user = User.query.filter_by(email = email).first()
        # if user:
        #     flash('Email already exists', category=error)
        # if len(email) < 4:
        #     flash('Email must be greater than 4 characters.', category = 'error')
        # elif len(firstname) < 2:
        #     flash('First name must be greater than 1 character.', category='error')
        # elif password1 != password2:
        #     flash('Passwords don\'t match.', category='error')
        # elif len(password1) < 7:
        #     flash('Password must be at least 7 characters.', category='error')
        # else: *create a user*
        #     flash('account created!', category='success')
        
    return render_template("signup.html")
