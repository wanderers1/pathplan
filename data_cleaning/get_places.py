import requests
import pandas as pd
import os

# Load your CSV file into a DataFrame
file_path = 'pathplan/database/data_cleaning/filtered_reviews.csv'
df = pd.read_csv(file_path)

# Your Google API key
API_KEY = 'AIzaSyDTwFxo3oSgft3rfbH2Ah98GAe9jGp9IXs'

# Function to get a photo URL from Google Places API
def get_place_photo(place, city):
    search_url = f"https://maps.googleapis.com/maps/api/place/findplacefromtext/json"
    search_params = {
        'input': f"{place}, {city}",
        'inputtype': 'textquery',
        'fields': 'photos',
        'key': API_KEY
    }
    response = requests.get(search_url, params=search_params)
    results = response.json().get('candidates')
    if results and 'photos' in results[0]:
        photo_reference = results[0]['photos'][0]['photo_reference']
        return f"https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference={photo_reference}&key={API_KEY}"
    return None

# Initialize a list to hold the image URLs
image_urls = []

# Fetch and save image URLs for each place
for index, row in df.iterrows():
    place = row['Place']
    city = row['City']
    photo_url = get_place_photo(place, city)
    if photo_url:
        image_urls.append(photo_url)
        print(f"Image URL found for {place}, {city}")
    else:
        image_urls.append(None)
        print(f"No image found for {place}, {city}")

# Add the image URLs as a new column in the DataFrame
df['Image_URL'] = image_urls

# Save the updated DataFrame back to the CSV file
df.to_csv(file_path, index=False)

print("Image URLs have been saved to the CSV file.")
