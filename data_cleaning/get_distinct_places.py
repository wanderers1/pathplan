import pandas as pd

# Path to your CSV file
file_path = 'pathplan/database/data_cleaning/filtered_reviews.csv'

# Load the CSV file
df = pd.read_csv(file_path)

# Get the distinct combinations of place names and their corresponding cities
distinct_places_df = df[['Place', 'City']].drop_duplicates()

# Save to a new CSV file
distinct_places_df.to_csv('distinct_places.csv', index=False)

print("Distinct places and their cities have been saved to 'distinct_places.csv'")
