import pandas as pd
from faker import Faker
from collections import defaultdict

# Initialize Faker
fake = Faker('en_IN')

# Define the number of rows
num_rows = 174221

# Define the maximum number of repetitions per name
max_repeats = 15

# Create a dictionary to count the repetitions of each name
name_counts = defaultdict(int)

# List to hold the names and emails
names_emails = []

# Generate names and emails ensuring no more than max_repeats for each name
while len(names_emails) < num_rows:
    name = fake.name()
    if name_counts[name] < max_repeats:
        email = fake.email()
        names_emails.append((name, email))
        name_counts[name] += 1

# Create a DataFrame from the generated names and emails
names_emails_df = pd.DataFrame(names_emails, columns=['name', 'email'])

# Read the existing reviews CSV file
reviews_df = pd.read_csv('pathplan/database/data_cleaning/filtered_reviews.csv')

# Ensure the file has exactly 160,000 rows
if len(reviews_df) != num_rows:
    raise ValueError(f"The reviews.csv file must have exactly {num_rows} rows.")

# Add the names and emails to the reviews DataFrame
reviews_df[['Name', 'Email']] = names_emails_df

# Write the updated DataFrame to a new CSV file
reviews_df.to_csv('pathplan/database/data_cleaning/filtered_reviews.csv', index=False)

print(f"Updated {num_rows} rows with names and emails and written to reviews_with_names_emails.csv")
