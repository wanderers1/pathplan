import pandas as pd
places_df = pd.read_csv('filtered_places.csv')
reviews_df = pd.read_csv('filtered_reviews.csv')
df_combined = pd.merge(reviews_df, places_df[['City', 'Place', 'Category']], on=['City', 'Place'], how='left')

# Display the combined DataFrame to verify
print(df_combined.head())

# Update your review database with the new category information
reviews_df['Category'] = df_combined['Category']


df_combined.to_csv('filtered_reviews.csv', index=False)