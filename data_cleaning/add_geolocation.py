import pandas as pd
from geopy.geocoders import Nominatim
from geopy.exc import GeocoderTimedOut
import time

# Function to geolocate a place with retries
def geolocate(place, city, retries=3):
    geolocator = Nominatim(user_agent="my_app")
    attempt = 0
    while attempt < retries:
        try:
            location = geolocator.geocode(f"{place}, {city}")
            if location:
                return location.latitude, location.longitude
            else:
                return None, None
        except GeocoderTimedOut:
            attempt += 1
            time.sleep(1)  # Wait for 1 second before retrying
    return None, None

# Read the CSV into a DataFrame
df = pd.read_csv('distinct_places.csv')

# Initialize lists with None values
latitudes = [None] * len(df)
longitudes = [None] * len(df)

# Geolocate each place in the DataFrame
for index, row in df.iterrows():
    place = row['Place']
    city = row['City']
    print(f"Geolocating {place}, {city} (index {index})...")
    lat, lon = geolocate(place, city)
    latitudes[index] = lat
    longitudes[index] = lon

    # Optionally save progress every 100 rows
    if index % 100 == 0:
        df['Latitude'] = latitudes
        df['Longitude'] = longitudes
        df.to_csv('pathplan/database/data_cleaning/filtered_reviews_temp.csv', index=False)

# Add latitude and longitude columns to the DataFrame
df['Latitude'] = latitudes
df['Longitude'] = longitudes

# Save the updated DataFrame back to CSV
df.to_csv('distinct_places.csv', index=False)
