import pandas as pd
from transformers import pipeline

# Load the dataset
places_df = pd.read_csv('distinct_places.csv')

# Initialize the zero-shot classification pipeline
classifier = pipeline('zero-shot-classification', model='facebook/bart-large-mnli')

# List of candidate labels
candidate_labels = [
    "Historical_Place",
    "Museum",
    "Temple",
    "Monument",
    "Garden",
    "Cultural_Center",
    "Palace",
    "Church",
    "Beach",
    "Market",
    "Zoo",
    "Science_Center",
    "Nature_Park",
    "Government_Building",
    "Art_Gallery",
    "Adventure_Park",
    "Entertainment",
    "Riverfront",
    "Sports_Venue",
    "Promenade",
    "Mosque",
    "University",
    "Wildlife Sanctuary"
]

def categorize_place(place_name):
    result = classifier(place_name, candidate_labels)
    best_label = result['labels'][0]
    return best_label

# Apply the categorization function and print progress every 100 rows
for i, place_name in enumerate(places_df['Place']):
    places_df.at[i, 'Category'] = categorize_place(place_name)
    if (i + 1) % 100 == 0:
        print(f"Processed {i + 1} rows")

# Save the categorized data to a CSV file
places_df.to_csv('categorized_places.csv', index=False)

print("Data has been categorized and saved to categorized_places.csv")
