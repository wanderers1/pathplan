import pandas as pd

# Read the filtered places CSV file
places_df = pd.read_csv('filtered_places.csv')

# Read the reviews CSV file
reviews_df = pd.read_csv('Review_db.csv')

# Filter reviews_df to only include places that are in places_df
filtered_reviews_df = reviews_df[reviews_df[['City', 'Place']].apply(tuple, axis=1).isin(places_df[['City', 'Place']].apply(tuple, axis=1))]

# Save the filtered reviews to a new CSV file (optional)
filtered_reviews_df.to_csv('filtered_reviews.csv', index=False)

print("Done")
