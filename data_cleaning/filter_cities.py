import pandas as pd

# Load your CSV data
df = pd.read_csv("Review_db.csv")

# List of 50 cities to keep (with corrections and some additions)
cities_to_keep = [
    
"Agra", "Bengaluru", "Chennai", "Delhi", "Goa", "Hyderabad", "Jaipur", "Kolkata", "Mumbai", "Udaipur","Ooty", "Haridwar"
]

# Filter the DataFrame (case-insensitive)
filtered_df = df[df['City'].str.lower().isin([city.lower() for city in cities_to_keep])]

# Handle missing values (if any)
filtered_df.dropna(subset=['City'], inplace=True)

# Save the filtered data
filtered_df.to_csv("filtered_cities_database.csv", index=False)
print("Done")