import pandas as pd
from datetime import datetime
from app.models import db, User, Place, Review
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
DB_NAME = "pathplan.db"

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = f'sqlite:///{DB_NAME}'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db.init_app(app)


df = pd.read_csv('data/Review_db.csv')


def clean_review_text(text):
    
    return text

def parse_date(date):
    if pd.isna(date):
        return None 
    return datetime.strptime(str(date), '%Y-%m-%d')

with app.app_context():
    db.create_all()
    users = df[['Name']].drop_duplicates().reset_index(drop=True)
    places = df[['Place', 'City']].drop_duplicates().reset_index(drop=True)

    for i, user in users.iterrows():
        db.session.add(User(id=i+1, firstname=user['Name']))
    for i, place in places.iterrows():
        db.session.add(Place(place_id=i+1, city_name=place['City'], place_name=place['Place']))
    db.session.commit()

    for _, row in df.iterrows():
        user = User.query.filter_by(name=row['Name']).first()
        place = Place.query.filter_by(place_name=row['Place']).first()
        review_date = parse_date(row['Date'])
        if review_date is None:
            continue
        review = Review(
            id = user.id,
            place_id = place.place_id,
            rating = row['Rating'],
            review_text = clean_review_text(row['Review']),
            review_date = review_date,
            raw_review = row['Raw_Review']
        )
        db.session.add(review)
    db.session.commit()
