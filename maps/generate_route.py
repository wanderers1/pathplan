import googlemaps
import gmplot

API_KEY = 'AIzaSyDTwFxo3oSgft3rfbH2Ah98GAe9jGp9IXs'
gmaps = googlemaps.Client(key=API_KEY)

places = [
    {'name': 'Gateway of India', 'lat': 18.9220, 'lng': 72.8347},
    {'name': 'Chhatrapati Shivaji Terminus', 'lat': 18.9398, 'lng': 72.8355},
    {'name': 'Marine Drive', 'lat': 18.9440, 'lng': 72.8238},
    {'name': 'Elephanta Caves', 'lat': 18.9633, 'lng': 72.9317},
    {'name': 'Haji Ali Dargah', 'lat': 18.9825, 'lng': 72.8101}
]

latitudes = [place['lat'] for place in places]
longitudes = [place['lng'] for place in places]

gmap = gmplot.GoogleMapPlotter(latitudes[0], longitudes[0], 13, apikey=API_KEY)

for place in places:
    gmap.marker(place['lat'], place['lng'], title=place['name'])

waypoints = [(lat, lng) for lat, lng in zip(latitudes, longitudes)]
gmap.plot(latitudes, longitudes, 'cornflowerblue', edge_width=10)

gmap.draw('pathplan/maps/map.html')

print("Map generated. Check 'map.html' in your current directory.")
